package com.dharmawan.user.service;

import com.dharmawan.user.model.db.User_master;
import com.dharmawan.user.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.*;

@Component
@Service
public class UserService {
    @Autowired
    UserRepo user_repo;

    public boolean isExist(int id) {
        Optional<User_master> user = user_repo.findById(id);
        return user.isPresent();
    }

    private ArrayList<Integer> removeDuplicate(ArrayList<Integer> list) {
        Set<Integer> set = new LinkedHashSet<>(list);
        list.clear();
        list.addAll(set);
        return list;
    }

    public ArrayList<User_master> getUser(Integer[] id) {
        ArrayList<Integer> idList = new ArrayList<>();

        Collections.addAll(idList, id);
        idList = removeDuplicate(idList);

        ArrayList<User_master> list = new ArrayList<>();
        for (Integer item : idList) {
            Optional<User_master> user = user_repo.findById(item);
            user.ifPresent(list::add);
        }
        return list;
    }
}
