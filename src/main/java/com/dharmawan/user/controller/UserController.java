package com.dharmawan.user.controller;

import com.dharmawan.user.model.Output;
import com.dharmawan.user.model.input.UserAuthenticate;
import com.dharmawan.user.model.input.UserGetById;
import com.dharmawan.user.service.UserService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/is-exist")
    public Output authenticate(@RequestBody UserAuthenticate userAuthenticate) {
        return new Output(userService.isExist(userAuthenticate.getId()));
    }

    @PostMapping("/get-by-id")
    public Output getById(@RequestBody UserGetById userGetById) {
        Gson g = new Gson();
        System.out.println(g.toJson(userGetById));

        Object data;

        if (userGetById == null || userGetById.getId().length == 0) {
            data = null;
        }
        else {
            data = userService.getUser(userGetById.getId());
        }
        return new Output(data);
    }
}
