package com.dharmawan.user.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Output {
    private Object data;

    public Output(Object data) {
        this.data = data;
    }
}
