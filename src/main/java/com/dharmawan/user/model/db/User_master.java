package com.dharmawan.user.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Entity
@Data
public class User_master {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotEmpty
    @Column(name="user_id")
    private int id;

    @NotEmpty
    @Column(name="user_nama")
    private String nama;

    @NotEmpty
    @Column(name="user_ttl")
    private String ttl;

    @NotEmpty
    @Column(name="user_divisi")
    private String divisi;

    @JsonIgnore
    @Column(name="user_last_edited")
    private String last_edited;
}
