package com.dharmawan.user.model.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserGetById {
    private Integer[] id;
}
