package com.dharmawan.user.model.input;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class UserAuthenticate {
    @NotEmpty
    private int id;
}
