package com.dharmawan.user.repository;

import com.dharmawan.user.model.db.User_master;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
@Repository
public interface UserRepo extends PagingAndSortingRepository<User_master, Integer> {

}
