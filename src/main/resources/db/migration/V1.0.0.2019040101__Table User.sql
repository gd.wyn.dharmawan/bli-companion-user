create table "user_master"
(
    user_id             serial not null
        constraint user_pk
            primary key,
    user_nama           text,
    user_ttl            text,
    user_divisi         text,
    user_last_edited    text default now()
);
